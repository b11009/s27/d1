// get = retrieve or reading resources
// post = sends data for creating
// put = updating resources
// delete = deleting resources

let http = require("http");

// create server
http.createServer(function(request, response){

	// GET Request to retrieve or read info
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database');
	}

	// POST Request to create or add info
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent in database');
	}

	
}).listen(4000);

console.log('Server running at localhost:4000');