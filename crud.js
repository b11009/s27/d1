let http = require("http");
const port = 4000;

// npx kill-port 4000

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@gmail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@gmail.com"
	}
];

http.createServer(function(request, response){
	// Route for returning all items
	if(request.url == "/users" && request.method == "GET"){
		// sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'application/json'});

		// retrieve directory array
		response.write(JSON.stringify(directory));

		response.end();
	}

	// Route for creating data

	if (request.url == "/users" && request.method == "POST"){
		// placeholder
		let requestBody = '';

		request.on('data', function(data){

			// requestBody = requestBody + data;
			requestBody += data;
		});

		// response end step (only runs after the request has completely been sent)
		request.on('end', function(){
			// typeof = check data type
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody)

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
}).listen(port);
console.log(`Server running at localhost:${port}`);